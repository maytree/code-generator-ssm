package cn.tzp.vo;

public class ColumnVo {

	
	private String javaType;			//类的字段类型
	private String dbType;				//数据库数据类型
	
	private String name;				//表中的列名
	private String fieldName;			//类中的字段名
	
	private String upperCaseColumnName;	//帕斯卡风格的列名
	private String comment;				//列注释
	public String getJavaType() {
		return javaType;
	}
	public void setJavaType(String javaType) {
		this.javaType = javaType;
	}
	public String getDbType() {
		return dbType;
	}
	public void setDbType(String dbType) {
		this.dbType = dbType;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public String getUpperCaseColumnName() {
		return upperCaseColumnName;
	}
	public void setUpperCaseColumnName(String upperCaseColumnName) {
		this.upperCaseColumnName = upperCaseColumnName;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	
	
	
	
}
