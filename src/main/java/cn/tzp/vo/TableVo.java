package cn.tzp.vo;

import java.util.ArrayList;
import java.util.List;

public class TableVo {

	private String className;	//帕斯卡风格类名
	private String camelName;	//骆驼风格类名，作为类的对象，mapper接口传参时使用
	private String tableName;	//下划线风格表名
	private String comment;		//表注释
	private List<ColumnVo> columns = new ArrayList<ColumnVo>();//表中所有的列
	
	
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}

	public String getCamelName() {
		return camelName;
	}

	public void setCamelName(String camelName) {
		this.camelName = camelName;
	}

	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public List<ColumnVo> getColumns() {
		return columns;
	}
	public void setColumns(List<ColumnVo> columns) {
		this.columns = columns;
	}
	
	
	
}
