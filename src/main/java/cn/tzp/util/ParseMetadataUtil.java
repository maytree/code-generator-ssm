package cn.tzp.util;

import cn.tzp.vo.ColumnVo;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by TangYang on 2018-12-17 18:58
 */
public class ParseMetadataUtil {


    private static Connection conn;
    private static DatabaseMetaData metaData; //元数据对象

    static{
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * 打开数据源
     */
    public static void openConnection(){
        try {
            if(conn == null || conn.isClosed()){
                conn = DriverManager.getConnection(
                        "jdbc:mysql://127.0.0.1:3306/itripdb?useUnicode=true&characterEncoding=utf-8",
                        "root",
                        "root");
                metaData = conn.getMetaData();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     */
    public static void closeAll(){
        if(conn != null){
            try {
                if(!conn.isClosed())
                    conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * 获取所有表的信息
     * @return
     */
    public static Map<String,String> getTables(){
        openConnection();
        Map<String,String> tables = new HashMap<>();
        ResultSet rs = null ;
        try {
            rs = metaData.getTables(null, null, null, new String[]{"TABLE"});
            while (rs.next()){
                tables.put(rs.getString("TABLE_NAME"),rs.getString("REMARKS"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tables;
    }

    /**
     * 根据表名获取对应的所有列的信息
     * @return
     */
    public static List<ColumnVo> getColumns(String tableName){
        List<ColumnVo> columns = new ArrayList<>();
        ResultSet rs = null;
        try {
            rs = metaData.getColumns(null, "%", tableName, "%");
            while (rs.next()){
                ColumnVo columnVo = new ColumnVo();
                columnVo.setComment(rs.getString("REMARKS"));
                columnVo.setDbType(rs.getString("TYPE_NAME"));
                columnVo.setName(rs.getString("COLUMN_NAME"));
                columns.add(columnVo);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return columns;
    }


}
