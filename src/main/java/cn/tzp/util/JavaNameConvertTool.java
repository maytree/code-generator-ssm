package cn.tzp.util;

import com.sun.org.apache.regexp.internal.RE;

public class JavaNameConvertTool {

	/**
	 *  将表名（数据库表名格式是下划线格式）转为帕斯卡格式命名或骆驼格式命名
	 *  将列名（数据库字段格式不是下划线格式）转为帕斯卡格式命名或骆驼格式命名
	 * @param name 表名或字段名
	 * @param isPascal 是否为帕斯卡命名
	 * @return
	 */
	private static String convert(String name,boolean isPascal) {
		StringBuffer result = new StringBuffer();
		String[] underArray = name.split("_");
		//如果要转换的字符串没有下划线
		if(underArray.length == 1) {
			if(isPascal) {
				return result.append(name.substring(0,1).toUpperCase())
						.append(name.substring(1)).toString();
			}
			return name;
		}
		//如果有下划线
		for (String str : underArray) {
			if(!isPascal && result.length() == 0) {
				result.append(str.substring(0,1).toLowerCase())
						.append(str.substring(1)).toString();
			}else {
				result.append(str.substring(0,1).toUpperCase());
				result.append(str.substring(1).toLowerCase());
			}
		}
		return result.toString();
	}

	/**
	 * 转为骆驼格式命名
	 */
	public static String toCamel(String name){
		return convert(name, false);
	}

	/**
	 * 转为帕斯卡格式命名
	 * @param name
	 * @return
	 */
	public static String toPascal(String name){
		return convert(name, true);
	}

	/**
	 * 将数据库字段类型转为Java类的字段类型
	 * @param dbType 传入的数据库字段类型
	 * @return
	 */
	public static String dbTypeToJavaType(String dbType){
		switch (dbType){
			case "INT":
				return "Integer";
			case "BIGINT":
				return "Long";
			case "VARCHAR":
				return "String";
			case "DATETIME":
				return "Date";
			case "DECIMAL":
				return "Double";
				default:return "String";
		}
	}
	

}
