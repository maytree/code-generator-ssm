package cn.tzp.generator;

import cn.tzp.vo.TableVo;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;

/**
 * Created by TangYang on 2018-12-17 19:59
 */
public class PojoCodeGenerator extends CodeGeneratorAbstract{


    public PojoCodeGenerator(){};

    public PojoCodeGenerator(String ftl) throws IOException {
        super(ftl);
    }


    @Override
    public void generateCode() {
        logger.info("============开始使用"+this.template.getName()+"模板====================");
        for (TableVo tableVo : tableVos) {
            //table  作为key值，与对应模板的 table一致
            valueMap.put("table", tableVo);
            OutputStreamWriter writer = null;
            try{
                File saveDir = new File(savePath);
                if(!saveDir.exists()){
                    saveDir.mkdir();
                }
                File generatorFile = new File(this.savePath+tableVo.getClassName()+this.fileNameSuffix);
                //如果存在旧文件就删除
                if(generatorFile.exists()){
                    generatorFile.delete();
                }
                //使用Freemarket将数据与模板合成，输出到要生成的代码文件中
                writer = new FileWriter(generatorFile);
                this.template.process(valueMap, writer);
            }catch(Exception e){
                e.printStackTrace();
                logger.info("============根据"+this.template.getName()+"模板生成无效"+tableVo.getClassName()+fileNameSuffix);
            }finally {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                logger.info("============根据"+this.template.getName()+"模板成功生成"+tableVo.getClassName()+fileNameSuffix);
            }
        }
        logger.info("\n-----------------------------------------------------------\n");
    }

}
