package cn.tzp.generator;

import cn.tzp.util.JavaNameConvertTool;
import cn.tzp.util.ParseMetadataUtil;
import cn.tzp.vo.ColumnVo;
import cn.tzp.vo.TableVo;
import freemarker.template.Configuration;
import freemarker.template.Template;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Created by TangYang on 2018-12-17 19:40
 */
public abstract class CodeGeneratorAbstract {

    protected Logger logger;
    protected Configuration config;
    protected Template template;
    protected Map valueMap;
    protected String savePath;
    protected String fileNameSuffix;
    protected List<TableVo> tableVos;

    public void setPackageName(String packageName) {
        valueMap.put("package", packageName);
    }

    public void setSavePath(String savePath) {
        this.savePath = savePath;
    }

    public void setFileNameSuffix(String fileNameSuffix) {
        this.fileNameSuffix = fileNameSuffix;
    }

    protected CodeGeneratorAbstract(){}

    protected CodeGeneratorAbstract(String ftl) throws IOException {
        config = new Configuration();
        config.setClassForTemplateLoading(this.getClass(), "/template");
        template = config.getTemplate(ftl);
        valueMap = new HashMap();
        tableVos = new ArrayList<>();
        logger = Logger.getLogger(this.getClass().getSimpleName());
        this.parseMetadata();
    }

    /**
     * 解析元数据获取表以及表所对应的所有列信息
     */
    private void parseMetadata() {
        Map<String, String> tables = ParseMetadataUtil.getTables();
        for (Map.Entry<String, String> entry : tables.entrySet()) {
            TableVo table = new TableVo();
            table.setTableName(entry.getKey());
            table.setComment(entry.getValue());
            table.setCamelName(JavaNameConvertTool.toCamel(entry.getKey()));
            table.setClassName(JavaNameConvertTool.toPascal(entry.getKey()));
            List<ColumnVo> columns = ParseMetadataUtil.getColumns(entry.getKey());
            for (ColumnVo column : columns) {
                column.setJavaType(JavaNameConvertTool.dbTypeToJavaType(column.getDbType()));
                column.setFieldName(JavaNameConvertTool.toCamel(column.getName()));
                column.setUpperCaseColumnName(JavaNameConvertTool.toPascal(column.getName()));
                table.getColumns().add(column);
            }
            tableVos.add(table);
        }
        ParseMetadataUtil.closeAll();
    }

    public abstract void generateCode();

}




