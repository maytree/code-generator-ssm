package cn.tzp.generator;

import cn.tzp.vo.ColumnVo;
import cn.tzp.vo.TableVo;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.Map;

/**
 * Created by TangYang on 2018-12-17 20:05
 */
public class MapperAndXmlGenerator extends CodeGeneratorAbstract {


    public MapperAndXmlGenerator(){}

    public MapperAndXmlGenerator(String ftl) throws IOException {
        super(ftl);
    }

    private void setSubPackage(String subPackage){
        valueMap.put("subPackage", subPackage);
    }

    @Override
    public void generateCode() {
        logger.info("============开始使用"+this.template.getName()+"模板====================");
        for (TableVo tableVo : tableVos) {
            valueMap.put("table", tableVo);
            OutputStreamWriter writer = null;
            try{
                File saveDir = new File(savePath+tableVo.getCamelName().toLowerCase());
                if(!saveDir.exists()){
                    saveDir.mkdir();
                }
                File generatorFile = new File(savePath+
                        tableVo.getCamelName().toLowerCase()+"\\"+tableVo.getClassName()+this.fileNameSuffix);
                if(generatorFile.exists()){
                    generatorFile.delete();
                }
                setSubPackage(tableVo.getCamelName().toLowerCase());
                writer = new FileWriter(generatorFile);
                //使用Freemarket将数据与模板合成，输出到要生成的代码文件中
                this.template.process(valueMap, writer);
            }catch(Exception e){
                e.printStackTrace();
                logger.info("============根据"+this.template.getName()+"模板生成无效"+tableVo.getClassName()+fileNameSuffix);
            }
            logger.info("============根据"+this.template.getName()+"模板成功生成"+tableVo.getClassName()+fileNameSuffix);
        }
        logger.info("\n-----------------------------------------------------------\n");
    }


}
