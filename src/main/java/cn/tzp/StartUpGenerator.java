package cn.tzp;

import cn.tzp.generator.CodeGeneratorAbstract;
import cn.tzp.generator.MapperAndXmlGenerator;
import cn.tzp.generator.PojoCodeGenerator;

/**
 * Created by TangYang on 2018-12-17 18:41
 */
public class StartUpGenerator {


    public static void main(String [] asdf) throws Exception{

        //先创建  保存  目录
//        CodeGeneratorAbstract codeGeneratorToPojo = new PojoCodeGenerator("pojo.ftl");
//        codeGeneratorToPojo.setPackageName("cn.pojo");
//        codeGeneratorToPojo.setSavePath("D:\\generator-code2\\pojo\\");
//        codeGeneratorToPojo.setFileNameSuffix(".java");
//        codeGeneratorToPojo.generateCode();

        CodeGeneratorAbstract codeGeneratorToMapper = new MapperAndXmlGenerator("mapper.ftl");
        codeGeneratorToMapper.setPackageName("cn.dao");
        codeGeneratorToMapper.setSavePath("D:\\generator-code2\\mapper\\");
        codeGeneratorToMapper.setFileNameSuffix("Mapper.java");
        codeGeneratorToMapper.generateCode();

        CodeGeneratorAbstract codeGeneratorToXml = new MapperAndXmlGenerator("sql.ftl");
        codeGeneratorToXml.setPackageName("cn.dao");
        codeGeneratorToXml.setSavePath("D:\\generator-code2\\mapper\\");
        codeGeneratorToXml.setFileNameSuffix("Mapper.xml");
        codeGeneratorToXml.generateCode();

    }
}
