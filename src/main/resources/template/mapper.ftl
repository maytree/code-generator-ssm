package ${package}.${subPackage};

import ${package?substring(0,2)}.pojo.${table.className};
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * ${table.className}映射器接口
 */
public interface ${table.className}Mapper {

    //根据id查询单个对象
    ${table.className} get${table.className}ById(@Param(value = "id") Long id) throws Exception;


    List<${table.className}> list${table.className}(${table.className} ${table.camelName}) throws Exception;

    //根据多个条件查询符合条件的总数量
    //Integer get${table.className}CountByMap(Map<String,Object> param) throws Exception;

    //插入一个对象，返回受影响的行数
    Integer insert${table.className}(${table.className} ${table.camelName}) throws Exception;

    //修改一个对象，返回受影响的行数
    Integer update${table.className}(${table.className} ${table.camelName}) throws Exception;

    //删除一个对象，返回受影响的行数
    Integer delete${table.className}ById(@Param(value = "id") Long id) throws Exception;
}
