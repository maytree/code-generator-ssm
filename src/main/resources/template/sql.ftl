<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<mapper namespace="${package}.${subPackage}.${table.className}Mapper">

    <select id="get${table.className}ById" resultType="${package?substring(0,2)}.pojo.${table.className}">
        select
        <trim suffixOverrides=",">
            <#list table.columns as col>
                ${col.name} as ${col.fieldName},
            </#list>
        </trim>
        from ${table.tableName}
        <trim prefix="where" prefixOverrides="and | or">
            <if test="id != null">
                and id=${r"#{"}id}
            </if>
        </trim>
    </select>

    <select id="list${table.className}" resultType="${package?substring(0,2)}.pojo.${table.className}">
        select
        <trim suffixOverrides=",">
            <#list table.columns as col>
                ${col.name} as ${col.fieldName},
            </#list>
        </trim>
        from ${table.tableName}
        <trim prefix="where" prefixOverrides="and | or">
            <#list table.columns as col>
                <if test="${col.fieldName} != null and ${col.fieldName} != ''">
                    and ${col.name}=${r"#{"}${col.fieldName}}
                </if>
            </#list>
        </trim>
    </select>

    <insert id="insert${table.className}">
        insert into ${table.tableName}(
        <trim suffixOverrides=",">
            <#list table.columns as col>
                ${col.name},
            </#list>
        </trim>
        )
        values(
        <trim suffixOverrides=",">
            <#list table.columns as col>
                ${r"#{"}${col.fieldName}},
            </#list>
        </trim>
        )
    </insert>

    <update id="update${table.className}">
        update ${table.tableName}
        <trim prefix="set" suffixOverrides="," suffix="where id = ${r"#{"}id}">
        <#list table.columns as col>
            <if test="${col.fieldName} !=null and ${col.fieldName}!=''">
                ${col.name}=${r"#{"}${col.fieldName}},
            </if>
        </#list>
        </trim>
    </update>

    <delete id="delete${table.className}ById">
        delete from ${table.tableName} where id = ${r"#{"}id}
    </delete>
</mapper>